<jsp:include page="_header.jsp" />
	<section class ="login">
		<div class ="login-box">
			<h4>${message}</h4>
			<form name="queryForm" action="register" method="post">
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label>Login</label>
							<input type="text" name="login" placeholder="${errors[0]}"/>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label>Password</label>
							<input type="password" name="password" placeholder=""/>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label>Email</label>
							<input type="text" name="email" placeholder="${errors[1]}"/>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label>Repeat Password</label>
							<input type="password" name="password-email" />
						</div>
					</div>
				</div>
				<button type="submit">Register</button>
			</form>
		</div>
	</section>
<jsp:include page="_footer.jsp" />	
<jsp:include page="_bottom.jsp" />	