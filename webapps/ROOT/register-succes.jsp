<jsp:include page="_header.jsp" />
	<section class ="register-success">
		<div class ="default-box">
			<h4>Thanks for register</h4>
			<div class="box">
				Thanks for register. Click the button to redirect to login.
				<a href="/login.jsp" class="default-button"></a>
			</div>
		</div>
	</section>
<jsp:include page="_footer.jsp" />	
<jsp:include page="_bottom.jsp" />	