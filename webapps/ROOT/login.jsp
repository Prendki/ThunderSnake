<jsp:include page="_header.jsp" />
	<section class ="login">
		<div class ="login-box">
			<h4>${message}</h4>
			<form name="queryForm" action="login" method="post">
				<div class="form-group">
					<label>Login</label>
					<input type="text" name="login" />
				</div>
				<div class="form-group">
					<label>Password</label>
					<input type="password" name="password" />
				</div>
				<button type="submit">Sign-in</button>
				<a href="/register" class="default-button r">Sign-up</a>
			</form>
		</div>
	</section>
<jsp:include page="_footer.jsp" />	
<jsp:include page="_bottom.jsp" />	
