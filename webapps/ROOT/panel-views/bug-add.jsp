<jsp:include page="/_header.jsp" />	
<jsp:include page="./_nav.jsp" />
<div class="panel-container">
	<jsp:include page="./sidebar.jsp" />
	<div class="panel-content">
		<div class="content">
			<h2>Add bug</h2>
			<section class="core">
				<form name="queryForm" action="/panel/bug/add" method="post">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<label>Problem Name</label>
										<textarea type="text" name="name"></textarea>
									</div>
								</div>
							</div>
							<button type="submit">Send</button>
						</div>
					</div>
				</form>
			</section>
		</div>
	</div>
</div>	
<jsp:include page="/_bottom.jsp" />	