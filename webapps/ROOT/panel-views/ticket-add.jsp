<jsp:include page="/_header.jsp" />
<jsp:include page="./_nav.jsp" />
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<div class="panel-container">
	<jsp:include page="./sidebar.jsp" />
	<div class="panel-content">
		<div class="content">
			<h2>Add ticket</h2>
			<section class="core">
				<form name="queryForm" action="/panel/ticket/add" method="post">
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label>ProblemName</label>
										<input type="text" name="name">
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label>Choose receiver</label>
										<select name="id_receiver">
											<c:forEach items="${users}" var="user">
												<option value="${user.id}">${user.login}</option>
											</c:forEach>
										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label>Choose bug</label>
										<select name="id_bug">
											<c:forEach items="${bugs}" var="bug">
												<option value="${bug.id}">${bug.name}</option>
											</c:forEach>
										</select>
									</div>
								</div>
							</div>
							<button type="submit">Send</button>
						</div>
					</div>
				</form>
			</section>
		</div>
	</div>
</div>
<jsp:include page="/_bottom.jsp" />