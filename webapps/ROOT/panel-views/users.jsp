<jsp:include page="/_header.jsp" />
<jsp:include page="./_nav.jsp" />
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<div class="panel-container">
	<jsp:include page="./sidebar.jsp" />
	<div class="panel-content">
		<div class="content">
			<h2>Users</h2>
			<section class="core">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>Id Account</th>
							<th>Login</th>
							<th>Email</th>
							<th>Role</th>
							<th>Data Created</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${users}" var="user">
							<tr>
								<td>${user.id}</td>
								<td>${user.login}</td>
								<td>${user.email}</td>
								<td>${user.role}</td>
								<td>${user.timestamp}</td>
								<td class="action">
									<form action="/panel/user/delete" method="POST">
										<button type="submit" name="id_user" value="${user.id}">Delete</button>
									</form>
									<form action="/panel/user/edit/role" method="GET">
										<button type="submit" name="id_user" value="${user.id}">Edit</button>
									</form>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</section>
		</div>
	</div>
</div>

<jsp:include page="/_bottom.jsp" />