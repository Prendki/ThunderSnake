<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ page import="models.User" %>
<nav>
	<div class="logo text-center">
		ThunderSnake
	</div>
	<form method="get" action="/login">
		<a href="/login">
			<button>Wyloguj</button>
		</a>
		<div class="pull-right login">
			<% User user=(User)session.getAttribute("user"); %>
			Zalogowany jako: 
			<strong><%= user.email %></strong>
			Rola:
			<strong><%= user.role %></strong>
		</div>

	</form>
	<div class="clearfix"></div>
</nav>