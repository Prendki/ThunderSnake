<jsp:include page="/_header.jsp" />
<jsp:include page="/panel-views/_nav.jsp" />
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<div class="panel-container">
	<jsp:include page="./sidebar.jsp" />
	<div class="panel-content">
		<div class="content">
			<h2>${message}</h2>
			<section class="core">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>Id</th>
							<th>Exhibitor</th>
							<th>Content</th>
							<th>Status</th>
							<th>Actions</th>
						</tr>
					</thead>			
					<tbody>
						<c:forEach items="${bugs}" var="bug">
							<tr>
								<td>
										${bug.id}
								</td>
								<td>
										${bug.exhibitor}
								</td>
								<td>
										${bug.name}
								</td>
								<td>
										${bug.status}
								</td>
								<td>
									<c:if test="${sessionScope.user.role == 'Manager'}">
										<form action="/panel/bug/delete" method="POST" class="display-inline-block">
											<button type="submit" name="id_bug" value="${bug.id}">Delete</button>
										</form>
									</c:if>
									<c:if test="${sessionScope.user.role == 'Manager' || sessionScope.user.role == 'Tester'}">
											<a href="/panel/bug/set?id_bug=${bug.id}&status=Tested" class="default green">Tested</a>
											<a href="/panel/bug/set?id_bug=${bug.id}&status=To fix" class="default green">To fix</a>
									</c:if>					
								</td>
							</tr>			
						</c:forEach>

					</tbody>
				</table>
				<a href="/panel/bug/add" class="default-button r">Add bug</a>
			</section>
		
		</div>
	</div>
</div>
<jsp:include page="/_bottom.jsp" />