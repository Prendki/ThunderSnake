<jsp:include page="/_header.jsp" />
<jsp:include page="/panel-views/_nav.jsp" />
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
	<div class="panel-container">
		<jsp:include page="./sidebar.jsp" />
		<div class="panel-content">
			<div class="content">
				<h2>${message}</h2>
				<section class="core">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>Id</th>
								<th>Content</th>
								<th>Status</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${tickets}" var="ticket">
								<tr>
									<td>
										${ticket.id}
									</td>
									<td>
										${ticket.name}
									</td>
									<td>
										${ticket.status}
									</td>
									<td>
										<c:if test="${sessionScope.user.role == 'Manager'}">
											<form action="/panel/ticket/delete" method="POST" class="display-inline-block">
												<button type="submit" name="id_ticket" value="${ticket.id}">Delete</button>
											</form>
										</c:if>
										<c:if test="${sessionScope.user.role == 'Developer' || sessionScope.user.role == 'Manager'}">
											<a href="/panel/ticket/check?id_ticket=${ticket.id}" class="default green">Check</a>
											<a href="/panel/ticket/set?id_ticket=${ticket.id}&status='fixed'" class="default green">Fixed</a>
										</c:if>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<c:if test="${sessionScope.user.role == 'Manager'}">
						<a href="/panel/ticket/add" class="default-button r">Add ticket</a>
					</c:if>
				</section>
			</div>
		</div>
	</div>
	<jsp:include page="/_bottom.jsp" />