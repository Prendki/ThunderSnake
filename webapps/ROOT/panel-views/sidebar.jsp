<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<div class="sidebar">
	<div class="sidebar-box">
		<div class="sidebar-top">
			<span>Navigation</span>
		</div>
		<ul class="sidebar-nav">
			<c:if test="${sessionScope.user.role != 'User'}">
				<li>
					<a href="/panel/tickets"><i class="fa fa-tasks" aria-hidden="true"></i> Tickets</a>
				</li>
			</c:if>
			<li>
				<a href="/panel/bugs"><i class="fa fa-bug" aria-hidden="true"></i> Bugs</a>
			</li>
			<c:if test="${sessionScope.user.role == 'Manager'}">
				<li>
					<a href="/panel/users"><i class="fa fa-users" aria-hidden="true"></i> Users</a>
				</li>
			</c:if>
		</ul>
		<div class="sidebar-top">
				<span>...in build</span>
			</div>
	</div>
</div>