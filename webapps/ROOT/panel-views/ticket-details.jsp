<jsp:include page="/_header.jsp" />
<jsp:include page="/panel-views/_nav.jsp" />
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
	<div class="panel-container">
		<jsp:include page="./sidebar.jsp" />
		<div class="panel-content">
			<div class="content">
				<h2>${message}</h2>
				<section class="core">
                        <h4 class="">
							Ticket ID : ${ticket.id}
						</h4>
						<h4 class="">
							Exhibiotr Name : ${ticket.exhibitor}
						</h4>
						<h4 class="">
							Receiver Name : ${ticket.receiver}
						</h4>
						<h4>
							Content Ticket
						</h4>
						<p>
							${ticket.name}
						</p>
						<h4>
							Content Associated Bug
						</h4>
						<p>
							${ticket.bugname}
						</p>
                </section>
            </div>
        </div>
	</div>
<jsp:include page="/_bottom.jsp" />