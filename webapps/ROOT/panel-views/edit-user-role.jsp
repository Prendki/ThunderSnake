<jsp:include page="/_header.jsp" />
<jsp:include page="./_nav.jsp" />
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
    <div class="panel-container">
        <jsp:include page="./sidebar.jsp" />
        <div class="panel-content">
            <div class="content">
                <h2>Edit user role</h2>
                <section class="core">
                    <form name="queryForm" action="/panel/user/edit/role" method="post">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Id User</label>
                                            <input type="text" name="id_user" value="${param.id_user}" />
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Set roles</label>
                                            <select name ="id_role">
                                                <c:forEach var="role" items="${roles}" varStatus="loop">
                                                    <option value="${loop.index}">
                                                        <c:out value="${role}" />
                                                    </option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit">Send</button>
                            </div>
                        </div>
                    </form>
                </section>
            </div>
        </div>
    </div>

    <jsp:include page="/_bottom.jsp" />