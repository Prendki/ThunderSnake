import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import models.User;
/**
 * Servlet implementation class UsersServlet
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public LoginServlet() {}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws 
	IOException,ServletException {
		PrintWriter out=response.getWriter();
		try {
	        String login = request.getParameter("login");
	        String password = request.getParameter("password");
			User user = new User();
			user.get(login,password);
			if(user.validate) {
				HttpSession session=request.getSession(); 
				session.setAttribute("user",user);
				session.setAttribute("id_user",user.id);
				session.setAttribute("name_user",user.email);
				response.sendRedirect("/panel");
			} else {
				String message = "Login : Wrong password";
        		request.setAttribute("message", message); // This will be available as ${message}
				request.getRequestDispatcher("/login.jsp").forward(request, response);
			}
		} catch(Exception e)
        {
		  	String message = "Login : Unexpected error, make sure that login is correct.";
        	request.setAttribute("message", message); // This will be available as ${message}
        	request.getRequestDispatcher("/login.jsp").forward(request, response);    
	    }
	}
	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String message = "Login";
        request.setAttribute("message", message); // This will be available as ${message}
        request.getRequestDispatcher("/login.jsp").forward(request, response);
    }
}
