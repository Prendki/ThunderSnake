import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.sql.ResultSet;
import java.sql.Statement;
import models.Users;
/**
 * Servlet implementation class UsersServlet
 */
@WebServlet("/panel/users")
public class UsersServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public UsersServlet() {}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws 
	IOException,ServletException {
	}
	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String message = "Users";
        Users users = new Users();
        users.get();
        request.setAttribute("message", message);
        request.setAttribute("users", users.users);
        request.getRequestDispatcher("/panel-views/users.jsp").forward(request, response);
    }
}