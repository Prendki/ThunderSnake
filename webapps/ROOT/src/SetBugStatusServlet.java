import java.io.IOException;
import java.io.PrintWriter;
import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.sql.Timestamp;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import models.HashGeneratorCompiler;
import models.Ticket;
import models.Bug;
/**
 * Servlet implementation class UsersServlet
 */
@WebServlet("/panel/bug/set")
public class SetBugStatusServlet extends HttpServlet {
    public SetBugStatusServlet() {}
	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String status = request.getParameter("status");
        String id_bug = request.getParameter("id_bug");
        Bug.setStatus(id_bug,status);
        if(status.equals("To fix")){
            Ticket.updateFromBug(id_bug,"To correct");
        }
        if(status.equals("Tested")){
            Ticket.updateFromBug(id_bug,"Checked");
        }
        response.sendRedirect("/panel/bugs");
    }
}
