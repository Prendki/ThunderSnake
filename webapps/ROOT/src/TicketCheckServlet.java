import java.io.IOException;
import java.io.PrintWriter;
import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.sql.Timestamp;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import models.HashGeneratorCompiler;
import models.Ticket;

/**
 * Servlet implementation class UsersServlet
 */
@WebServlet("/panel/ticket/check")
public class TicketCheckServlet extends HttpServlet {
    public TicketCheckServlet() {}

	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String message = "Ticket Details";
        String id_ticket = request.getParameter("id_ticket");
        Ticket ticket = new Ticket();
        ticket.get(Integer.parseInt(id_ticket));
        request.setAttribute("ticket",ticket);
        request.setAttribute("message", message); // This will be available as ${message}
        request.getRequestDispatcher("/panel-views/ticket-details.jsp").forward(request, response);
    }
}