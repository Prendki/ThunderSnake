import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import models.Result;
import models.Tickets;
/**
 * Servlet implementation class UsersServlet
 */
@WebServlet("/panel/tickets")
public class TicketsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public TicketsServlet() {}
	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out=response.getWriter();
        String message = "Tickets";
        request.setAttribute("message", message);
        try {
            Tickets tickets = new Tickets();
            tickets.get();
            request.setAttribute("tickets",tickets.tickets);
		} catch(Exception e)
        {
	       out.println(e);       
	    }
        request.getRequestDispatcher("/panel-views/tickets.jsp").forward(request, response);
    }
}
