import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.ArrayList;
import models.Result;
import models.Bugs;
/**
 * Servlet implementation class UsersServlet
 */
@WebServlet("/panel/bugs")
public class BugsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public BugsServlet() {}
	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out=response.getWriter();
        String message = "Bugs";
        request.setAttribute("message", message); // This will be available as ${message}
        try {
            Bugs bugs = new Bugs();
            bugs.get();
            request.setAttribute("bugs",bugs.bugs );
		} catch(Exception e)
        {
	       out.println(e);       
	    }
        request.getRequestDispatcher("/panel-views/bugs.jsp").forward(request, response);
    }
}
