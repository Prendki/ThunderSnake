import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.sql.ResultSet;
import java.sql.Statement;
import models.*;
/**
 * Servlet implementation class UsersServlet
 */
@WebServlet("/panel/user/delete")
public class DeleteUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public DeleteUserServlet() {}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws 
	IOException,ServletException {
        String id_user = request.getParameter("id_user");
        User.delete(Integer.parseInt(id_user));
        response.sendRedirect("/panel/users");
	}
}
