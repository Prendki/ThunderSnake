import java.io.IOException;
import java.io.PrintWriter;
import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.sql.Timestamp;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import models.HashGeneratorCompiler;
import models.Ticket;
import models.Bug;
/**
 * Servlet implementation class UsersServlet
 */
@WebServlet("/panel/ticket/set")
public class SetTicketStatusServlet extends HttpServlet {
    public SetTicketStatusServlet() {}
	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String status = request.getParameter("status");
        String id_ticket = request.getParameter("id_ticket");
        Ticket.setStatus(id_ticket,status);
        Ticket ticket = new Ticket();
        ticket.get(Integer.parseInt(id_ticket));
        String temp = ""+ticket.id_bug;
        Bug.setStatus(temp,"Test");

        response.sendRedirect("/panel/tickets");
    }
}
