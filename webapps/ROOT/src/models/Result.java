package models;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

public class Result {
    HashMap<String, HashMap<String, String>> table = new HashMap<String, HashMap<String, String>>();
    HashMap<String, String> row = new HashMap<String,String>();
    public Result(ResultSet rs,ResultSetMetaData rsmd) {
        try {
            table = new HashMap<String, HashMap<String, String>>();
            String id = "0";
            while(rs.next()){
                row = new HashMap<String,String>();
                for(int i = 1 ; i <= rsmd.getColumnCount(); i++) {
                    String c_name = rsmd.getColumnName(i);
                    String c_value = rs.getString(i);
                    System.out.println(c_name.length());
                    if(c_name.length() > 2){
                        row.put(c_name,c_value);
                    } else {
                        id=rs.getString(i);
                    }
                }
                table.put(id,row);
            }
		} catch(Exception e) {
	       System.out.println(e);       
	    }
    }
    public HashMap<String, HashMap<String, String>> getArrayList() {
        return table;
    }
     

}
/*
    Map<Integer,Map<String, String>> table = new Map<Integer,Map<String, String>>();
    public Result(ResultSet rs,ResultSetMetaData rsmd) {
        try {
            table = new ArrayList<Map<String, String>>();
            int id = 0;
            while(rs.next()){
                Map<String, String> row = new HashMap<String, String>();
                for(int i = 1 ; i <= rsmd.getColumnCount(); i++) {
                    row.put(rsmd.getColumnName(i),rs.getString(i));
                }
                table.put(id,row);
                id += 1;
            }
		} catch(Exception e) {
	       System.out.println(e);       
	    }
    }
    public ArrayList getArrayList() {
        return table;
    }
     



*/