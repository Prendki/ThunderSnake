package models;

import java.sql.ResultSet;
import java.util.ArrayList;
public class Column {
    public String name = null;
    public String value = null;
    public Column(String n,String v) {
        this.name = n;
        this.value = v;
    }
    String getName() {
        return name;
    }
    String getValue() {
        return value;
    }
}