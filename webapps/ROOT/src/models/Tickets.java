package models;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import main.ConnectionModule;
public class Tickets {
    public List<Ticket> tickets = new ArrayList<Ticket>();
    public Tickets() {}
    public void get() {
        try {
            //connection
            Class.forName("com.mysql.jdbc.Driver");
            String query = "SELECT * from tickets;";
            Statement stmt = ConnectionModule.getConnection().createStatement();
            ResultSet result = stmt.executeQuery(query);                                   
            while(result.next()){
                Ticket ticket = new Ticket();
                ticket.create(result);
                this.tickets.add(ticket);
            }
        } catch(Exception e) {
            System.out.println(e);
        }
    }
    

}