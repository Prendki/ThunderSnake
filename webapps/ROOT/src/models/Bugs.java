package models;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import main.ConnectionModule;
public class Bugs {
    public List<Bug> bugs = new ArrayList<Bug>();
    public Bugs() {}
    public void get() {
        try {
            //connection
            Class.forName("com.mysql.jdbc.Driver");
            String query = "SELECT *, ex.login as exhibitor_name FROM bugs LEFT JOIN users as ex ON bugs.id_exhibitor = ex.id;";
            Statement stmt = ConnectionModule.getConnection().createStatement();
            ResultSet result = stmt.executeQuery(query);                                   
            while(result.next()){
                Bug bug = new Bug();
                bug.create(result);
                this.bugs.add(bug);
            }
        } catch(Exception e) {
            System.out.println(e);
        }
    }
    

}