package models;
import models.HashGeneratorCompiler;
import java.sql.ResultSet;
import java.sql.Statement;
import main.ConnectionModule;
import models.Role;

public class Ticket {
    public Number id;
    public String name;
    public String exhibitor;
    public String status;
    public String receiver;
    public String bugname;
    public Number id_bug;
    public Number id_exhibitor;
    public Number id_receiver;
    public Ticket() {}
    public void get(Number id_ticket) {
        try {
            //connection
            Class.forName("com.mysql.jdbc.Driver");
            String query = "SELECT tickets.*, ex.login as exhibitor_name, rec.login as receiver_login,bg.id as id_bug,bg.name as bug_name "+
            "FROM tickets "+
            "LEFT JOIN users AS rec ON tickets.id_receiver = rec.id "+
            "LEFT JOIN users AS ex ON tickets.id_exhibitor = ex.id "+
            "LEFT JOIN bugs AS bg ON tickets.id_bug = bg.id "+
            "WHERE tickets.id = "+id_ticket+";";
            Statement stmt = ConnectionModule.getConnection().createStatement();
            ResultSet result = stmt.executeQuery(query);                                   
            if(result.next()) {
                this.id = result.getInt("id");
                this.name = result.getString("name");
                this.id_exhibitor = result.getInt("id_exhibitor");
                this.status = result.getString("status");
                this.id_receiver = result.getInt("id_receiver");
                this.bugname = result.getString("bug_name");
                this.id_bug = result.getInt("id_bug");
                this.exhibitor = result.getString("exhibitor_name");
                this.receiver = result.getString("receiver_login");
                stmt.close();
            }
        } catch(Exception e) {
            System.out.println(e);
        }
    }
    public void create (ResultSet result) {
        try{
            this.id = result.getInt("id");
            this.name = result.getString("name");
            this.status = result.getString("status");
            this.id_exhibitor = result.getInt("id_exhibitor");
            this.id_receiver = result.getInt("id_receiver");
        } catch(Exception e) {
            System.out.print(e);
        }
    }
    public static void add(String exhibitor,String name,String id_receiver,String id_bug) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
	        String query = "INSERT into tickets (name,id_exhibitor,id_receiver,id_bug,status) values('"+name+"','"+exhibitor+"','"+id_receiver+"','"+id_bug+"','created');";
			Statement stmt = ConnectionModule.getConnection().createStatement();
			stmt.execute(query);
			stmt.close();
        } catch(Exception e) {
            System.out.print(e);
        }
    }
    public static void setStatus(String id_ticket,String status){
        try {
			Class.forName("com.mysql.jdbc.Driver");
	        String query = "UPDATE tickets set status = "+status+" WHERE id = "+id_ticket+";";
			Statement stmt = ConnectionModule.getConnection().createStatement();
			stmt.executeUpdate(query);
		} catch(Exception e)
        {
			System.out.println(e);  
	    }
    }
    public static void updateFromBug(String id_bug,String status){
        try {
			Class.forName("com.mysql.jdbc.Driver");
	        String query = "UPDATE tickets set status = '"+status+"' WHERE id_bug = "+id_bug+";";
			Statement stmt = ConnectionModule.getConnection().createStatement();
			stmt.executeUpdate(query);
		} catch(Exception e)
        {
			System.out.println(e);  
	    }
    }
    public String getExhibitor() {
        return this.exhibitor;
    }
    public String getStatus() {
        return this.status;
    }
    public Number getId() {
        return this.id;
    }
    public String getName() {
        return this.name;
    }
    public String getBugname() {
        System.out.println(this.bugname);
        System.out.println("TEST ____");
        return this.bugname;
    }
    public String getReceiver() {
        return this.receiver;
    }
}
