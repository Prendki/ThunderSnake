package models;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import main.ConnectionModule;
public class Users {
    public List<User> users = new ArrayList<User>();
    public Users() {}
    public void get() {
        try {
            //connection
            Class.forName("com.mysql.jdbc.Driver");
            String query = "SELECT * from users;";
            Statement stmt = ConnectionModule.getConnection().createStatement();
            ResultSet result = stmt.executeQuery(query);                                   
            while(result.next()){
                User user = new User();
                user.create(result);
                this.users.add(user);
            }
        } catch(Exception e) {
            System.out.println(e);
        }
    }

}