package models;

public class Role {
    Role() {}
   public static String[] roles = {
        "User",
        "Tester",
        "Developer",
        "Manager"
    };
    public static String get(Integer id) {
        String role_name="brak";
        switch (id) {
            case 0:  role_name = "User";
                     break;
            case 1:  role_name = "Tester";
                     break;
            case 2:  role_name = "Developer";
                     break;
            case 3:  role_name = "Manager";
                     break;
            default:role_name = "None";
                     break;
        }
        return role_name;
    }
}