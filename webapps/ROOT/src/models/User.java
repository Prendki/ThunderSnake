package models;
import models.HashGeneratorCompiler;
import java.sql.ResultSet;
import java.sql.Statement;
import main.ConnectionModule;
import models.Role;

public class User {
    public Number id;
    public String login;
    public String email;
    public String role;
    public Integer id_role;
    public String timestamp;
    public Boolean validate = true;
    public User() {}
    public void get(String login,String password) {
        HashGeneratorCompiler hash = new HashGeneratorCompiler();
        try {
            //connection
            Class.forName("com.mysql.jdbc.Driver");
            String query = "SELECT * from users WHERE login='"+login+"';";
            Statement stmt = ConnectionModule.getConnection().createStatement();
            ResultSet result = stmt.executeQuery(query);                                   
            if(this.validate(result.next(),password,result)) {
                this.id = result.getInt("id");
                this.login = result.getString("login");
                this.email = result.getString("email");
                this.id_role = result.getInt("role");
                this.role = Role.get(result.getInt("role"));
                this.timestamp = result.getString("timestamp");
                stmt.close();
            }
        } catch(Exception e) {
            System.out.println(e);
        }
    }
    public Boolean validate(Boolean exist,String password_user,ResultSet result){
        HashGeneratorCompiler hash = new HashGeneratorCompiler();
        String crypted_password;
        String password_hashed;
        try{
            crypted_password = hash.get_SHA_512_SecurePassword(password_user,result.getString("timestamp"));
            password_hashed = result.getString("password");
            if(!crypted_password.equals(password_hashed)) {
                this.validate=false;
                return false;
            }
        } catch(Exception e) {
            System.out.print(e);
            this.validate=false;
            return false;
        }
        if(!exist) {
            this.validate=false;
            return false;
        }
        return true;
    }
    public void create (ResultSet result) {
        try{
            this.id = result.getInt("id");
            this.login = result.getString("login");
            this.email = result.getString("email");
            this.id_role = result.getInt("role");
            this.role = Role.get(result.getInt("role"));
            this.timestamp = result.getString("timestamp");
        } catch(Exception e) {
            System.out.print(e);
        }
    }
    public static void delete(Number id_user){
        try {
			Class.forName("com.mysql.jdbc.Driver");
	        String query = "DELETE from users WHERE id = "+id_user+";";
			Statement stmt = ConnectionModule.getConnection().createStatement();
			stmt.executeUpdate(query);
		} catch(Exception e)
        {
			System.out.println("ERROR");  
	    }
    }
    public static void setRole(String id_user,String id_role){
        try {
			Class.forName("com.mysql.jdbc.Driver");
	        String query = "UPDATE users set role = "+id_role+" WHERE id = "+id_user+";";
			Statement stmt = ConnectionModule.getConnection().createStatement();
			stmt.executeUpdate(query);
		} catch(Exception e)
        {
			System.out.println("ERROR");  
	    }
    }
    public String getEmail() {
        return this.email;
    }
    public String getLogin() {
        return this.login;
    }
    public Number getId() {
        return this.id;
    }
    public String getRole() {
        return this.role;
    }
    public String getTimestamp() {
        return this.timestamp;
    }
}