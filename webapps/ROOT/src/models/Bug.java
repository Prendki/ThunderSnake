package models;
import models.HashGeneratorCompiler;
import java.sql.ResultSet;
import java.sql.Statement;
import main.ConnectionModule;
import models.Role;

public class Bug {
    public Number id;
    public String exhibitor;
    public String name;
    public String status;
    public Number id_exhibitor;
    public Bug() {}
    public void get(Number id_bug) {
        try {
            //connection
            Class.forName("com.mysql.jdbc.Driver");
            String query = "SELECT bugs.*, ex.login as exhibitor_name"+
            "FROM bugs "+
            "LEFT JOIN users AS ex ON bugs.id_exhibitor = ex.id "+
            "WHERE bugs.id = "+id_bug+";";
            Statement stmt = ConnectionModule.getConnection().createStatement();
            ResultSet result = stmt.executeQuery(query);                                   
            if(result.next()) {
                this.id = result.getInt("id");
                this.name = result.getString("name");
                this.status = result.getString("status");
                this.id_exhibitor = result.getInt("id_exhibitor");
                this.exhibitor = result.getString("exhibitor_name");
                stmt.close();
            }
        } catch(Exception e) {
            System.out.println(e);
        }
    }
    public void create (ResultSet result) {
        try{
            this.id = result.getInt("id");
            this.name = result.getString("name");
            this.id_exhibitor = result.getInt("id_exhibitor");
            this.status = result.getString("status");
            this.exhibitor = result.getString("exhibitor_name");
        } catch(Exception e) {
            System.out.print(e);
        }
    }
    public static void setStatus(String id_bug,String status){
        try {
			Class.forName("com.mysql.jdbc.Driver");
	        String query = "UPDATE bugs set status = '"+status+"' WHERE id = "+id_bug+";";
			Statement stmt = ConnectionModule.getConnection().createStatement();
			stmt.executeUpdate(query);
		} catch(Exception e)
        {
			System.out.println(e);  
	    }
    }
    public void add() {}

    public String getExhibitor() {
        return this.exhibitor;
    }
    public Number getId() {
        return this.id;
    }
    public String getName() {
        return this.name;
    }
    public String getStatus() {
        return this.status;
    }
}
