import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Servlet implementation class UsersServlet
 */
@WebServlet("/panel/bug/delete")
public class DeleteBugServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public DeleteBugServlet() {}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws 
	IOException,ServletException {
		PrintWriter out=response.getWriter();
		try {
			System.out.println("Delete");
			Class.forName("com.mysql.jdbc.Driver");
	        String id_bug = request.getParameter("id_bug");
	        String query = "DELETE from bugs WHERE id = "+id_bug+";";
			Statement stmt = DBConnection.getConnection().createStatement();
			stmt.executeUpdate(query);
			response.sendRedirect("/panel/bugs");
		} catch(Exception e)
        {
			System.out.println("ERROR");
		   //response.sendRedirect("/login");
	       out.println(e);       
	    }
	}
}
