import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Servlet implementation class UsersServlet
 */
@WebServlet("/panel/ticket/delete")
public class DeleteTicketServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public DeleteTicketServlet() {}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws 
	IOException,ServletException {
		PrintWriter out=response.getWriter();
		try {
			System.out.println("Delete");
			Class.forName("com.mysql.jdbc.Driver");
	        String id_ticket = request.getParameter("id_ticket");
	        String query = "DELETE from tickets WHERE id = "+id_ticket+";";
			Statement stmt = DBConnection.getConnection().createStatement();
			stmt.executeUpdate(query);
			response.sendRedirect("/panel/tickets");
		} catch(Exception e)
        {
			System.out.println("ERROR");
		   //response.sendRedirect("/login");
	       out.println(e);       
	    }
	}
}
