import java.io.IOException;
import java.io.PrintWriter;
import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.sql.Timestamp;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import models.HashGeneratorCompiler;
import models.Role;
import models.User;
/**
 * Servlet implementation class UsersServlet
 */
@WebServlet("/panel/user/edit/role")
public class EditUserRoleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public EditUserRoleServlet() {}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws 
	IOException,ServletException {
		PrintWriter out=response.getWriter();
		try {
            String id_user = request.getParameter("id_user");
            String id_role = request.getParameter("id_role");
            User.setRole(id_user,id_role);
			response.sendRedirect("/panel/users");
		} catch(Exception e)
       {
			out.println(e);       
	   }
	}
	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String message = "Register";
        request.setAttribute("roles",Role.roles);
        request.setAttribute("message", message); // This will be available as ${message}
        request.getRequestDispatcher("/panel-views/edit-user-role.jsp").forward(request, response);
    }
}
