import java.io.IOException;
import java.io.PrintWriter;
import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.sql.Timestamp;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import models.HashGeneratorCompiler;
/**
 * Servlet implementation class UsersServlet
 */
@WebServlet("/register")
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public RegisterServlet() {}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws 
	IOException,ServletException {
		PrintWriter out=response.getWriter();
		try {
	        Class.forName("com.mysql.jdbc.Driver");
			/*params */
	        String login = request.getParameter("login");
	    	Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			String ts = timestamp.toString();
		    String password = request.getParameter("password");
	        String email = request.getParameter("email");
	        String passwordR = request.getParameter("password-repeat");
			HashGeneratorCompiler hash = new HashGeneratorCompiler();
			String crypted_password = hash.get_SHA_512_SecurePassword(password,ts);
			/* zapytania */
			String query1 = "SELECT * from users where login='"+login+"';";
	        String query = "INSERT into users (login,password,email,timestamp) values('"+login+"','"+crypted_password+"','"+email+"','"+ts+"');";
			String query3 = "SELECT * from users where email='"+email+"';";
			/* polaczenie */
			Statement stmt = DBConnection.getConnection().createStatement();
			ResultSet resultSet;
			/* zmienne */
			List<String> errors = new ArrayList<String>();

			/*Walidacja */
			resultSet = stmt.executeQuery(query1);
			if(resultSet.next()){
				errors.add("ERROR:Podany uzytkownik juz istnieje");
			}
			
			resultSet = stmt.executeQuery(query3);
			if(resultSet.next()){
				errors.add("ERROR:Podany mail juz istnieje");
			}
			if(errors.size() > 0){
				request.setAttribute("message","Register: Error");
				request.setAttribute("kupa","asdasdasd");
				request.setAttribute("errors",errors); // This will be available as ${message}
				
        		request.getRequestDispatcher("/register.jsp").forward(request, response);
			}else{
				stmt.execute(query);
				response.sendRedirect("login");
			}
			stmt.close();
			
		} catch(Exception e)
        {
	       out.println(e);       
	    }
	}
	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String message = "Register";
        request.setAttribute("message", message); // This will be available as ${message}
        request.getRequestDispatcher("/register.jsp").forward(request, response);
    }
}
