import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.sql.Statement;

import models.*;
/**
 * Servlet implementation class UsersServlet
 */
@WebServlet("/panel/ticket/add")
public class AddTicketServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public AddTicketServlet() {}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws 
	IOException,ServletException {
		try {
	        Class.forName("com.mysql.jdbc.Driver");
	        HttpSession session = request.getSession(true);
	        String exhibitor = session.getAttribute("id_user").toString();
			String name = request.getParameter("name");
			String id_receiver = request.getParameter("id_receiver");
			String id_bug = request.getParameter("id_bug");
			Ticket.add(exhibitor,name,id_receiver,id_bug);
			response.sendRedirect("/panel/tickets");
		} catch(Exception e)
       {
			System.out.println(e);       
	   }
	}
	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String message = "Dodaj ticket";
		Bugs bugs = new Bugs();
            bugs.get();
            request.setAttribute("bugs",bugs.bugs );
		Users users = new Users();
		users.get();
            request.setAttribute("users",users.users );
        request.setAttribute("message", message); // This will be available as ${message}
        request.getRequestDispatcher("/panel-views/ticket-add.jsp").forward(request, response);
    }
}
