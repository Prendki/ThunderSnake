import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.sql.ResultSet;
import java.sql.Statement;

/**
 * Servlet implementation class UsersServlet
 */
@WebServlet("/panel")
public class PanelServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public PanelServlet() {}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws 
	IOException,ServletException {

	}
	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String message = "Login";
        request.setAttribute("message", message); // This will be available as ${message}
        request.getRequestDispatcher("/panel-views/user.jsp").forward(request, response);
    }
}
