import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.sql.Statement;

/**
 * Servlet implementation class UsersServlet
 */
@WebServlet("/panel/bug/add")
public class AddBugServlet extends HttpServlet {

	public AddBugServlet() {}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws 
	IOException,ServletException {
		PrintWriter out=response.getWriter();
		try {
	        Class.forName("com.mysql.jdbc.Driver");
	        HttpSession session = request.getSession(true);
	        String exhibitor = session.getAttribute("id_user").toString();
	        String name = request.getParameter("name");
	        String status = "created";
	        String query = "INSERT into bugs (name,status,id_exhibitor) values('"+name+"','"+status+"','"+exhibitor+"');";
			Statement stmt = DBConnection.getConnection().createStatement();
			stmt.execute(query);
			stmt.close();
			response.sendRedirect("/panel/bugs");
		} catch(Exception e)
       {
			out.println(e);       
	   }
	}
	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String message = "Register";
        request.setAttribute("message", message); // This will be available as ${message}
        request.getRequestDispatcher("/panel-views/bug-add.jsp").forward(request, response);
    }
}
