#Compile files
javac -cp $CLASSPATH:$CATALINA_HOME/lib/* -d ../WEB-INF/classes/ *

or move libs to java directory 
default : /usr/lib/jvm/java-8-openjdk-amd64/jre/lib/ext

#Start project 
./bin/startup.sh; tail -f ./logs/catalina.out

#End project 
./bin/shutdown.sh

#ADD TO FILEVARIABLES
export CATALINA_HOME=/var/www/html/apache-tomcat-8.5.24
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64

#TOMCAT  VERSION 8.5.24
LIB JSTL REQUIRED !

#BACKUP
database_backup